package com.example.medicare.medicure;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by flazer on 19/11/2015.
 */
public class DateFormatter {

    private Date date;
    private String dateString;

    public DateFormatter(Date d){
        this.date = d;
    }

    public String getStringDate(){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int month = c.get(Calendar.MONTH);
        String monthString = "";

        if (month==0)
            monthString="January";
        else if(month==1)
            monthString="February";
        else if(month==2)
            monthString="March";
        else if(month==3)
            monthString="April";
        else if(month==4)
            monthString="May";
        else if(month==5)
            monthString="June";
        else if(month==6)
            monthString="July";
        else if (month==7)
            monthString="August";
        else if(month==8)
            monthString="September";
        else if(month==9)
            monthString="October";
        else if (month==10)
            monthString="November";
        else if (month==11)
            monthString="December";
        else
            monthString = "Invalid Month";

        dateString = Integer.toString(c.get(Calendar.DATE))+" "+monthString+" "+c.get(Calendar.YEAR);
        return dateString;
    }
}
