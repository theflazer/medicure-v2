package com.example.medicare.medicure.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medicare.medicure.Prescription;
import com.example.medicare.medicure.R;
import com.example.medicare.medicure.UserHandler;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ProfilePageActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView nameHolder;
    EditText username;
    EditText profile_dob;
    EditText username_bloodtype;
    EditText username_allergies;
    EditText username_emergencyName;
    EditText username_emergencyNo;
    EditText username_emergencyRel;
    UserHandler userHandler;
    Intent thisIntent;
    ImageView imageView;
    NavigationView navigationView;
    TextView navName;
    TextView navRole;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        System.gc();
        thisIntent = getIntent();
        navigationView = (NavigationView)findViewById(R.id.nav_view);
        nameHolder = (TextView)findViewById(R.id.fullnameEditText);
        username = (EditText)findViewById(R.id.profile_username);
        username.setEnabled(false);
        profile_dob = (EditText)findViewById(R.id.profile_dob);
        username_bloodtype = (EditText)findViewById(R.id.username_bloodtype);
        username_allergies = (EditText)findViewById(R.id.username_allergies);
        username_emergencyName = (EditText)findViewById(R.id.username_emergencyName);
        username_emergencyNo = (EditText)findViewById(R.id.username_emergencyNo);
        username_emergencyRel = (EditText)findViewById(R.id.username_emergencyRel);
        userHandler = new UserHandler(ParseUser.getCurrentUser());

        View header = navigationView.inflateHeaderView(R.layout.nav_header_prescription);
        navName = (TextView) header.findViewById(R.id.nav_name);
        navRole = (TextView) header.findViewById(R.id.nav_role);
        String role = ParseUser.getCurrentUser().getString("role");
        navName.setText(ParseUser.getCurrentUser().getString("name"));
        navRole.setText(role.substring(0, 1).toUpperCase() + role.substring(1));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        imageView = (ImageView)findViewById(R.id.imageView4);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProfilePageActivity.this, QRGeneratorActivity.class);
                startActivity(i);

            }
        });

        letsRelate();


    }

    @Override
    public void onResume() {
        super.onResume();
        username.setText(userHandler.getUsername());
        nameHolder.setText(userHandler.getFullname());
        profile_dob.setText(userHandler.getDoB());
        username_bloodtype.setText(userHandler.getBloodType());
        username_allergies.setText(userHandler.getAllergies());
        username_emergencyName.setText(userHandler.getEmergencyName());
        username_emergencyNo.setText(userHandler.getEmergencyNo());
        username_emergencyRel.setText(userHandler.getEmergencyRel());

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    public void letsRelate(){
        ParseObject parseObject = new ParseObject("visits");
        parseObject.put("patientID",ParseUser.getCurrentUser());
        parseObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("Visits", "Visit saved");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.profile_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.profile_nav_profile){
        }
        else if (id == R.id.profile_nav_prescriptions)
        {
            Intent i = new Intent(ProfilePageActivity.this, PrescriptionActivity.class);
            startActivity(i);
            finish();
        }
        else if(id == R.id.profile_nav_logout){
            Log.i("LogOut", "Log Out Command issued");
            ParseUser.logOut();
            Intent logOutIntent = new Intent(ProfilePageActivity.this, LoginActivity.class);
            logOutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutIntent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}


