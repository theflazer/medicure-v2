package com.example.medicare.medicure.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.medicare.medicure.Medicine;
import com.example.medicare.medicure.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MedicineActivity extends AppCompatActivity {

    private Date prescriptionDate;
    private ParseObject prescriptionOb;
    private Calendar calendar = Calendar.getInstance();
    private String date;
    private String prescriptionId;
    private List<Medicine> Medicines;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Intent intent = getIntent();
        date = intent.getExtras().getString("date");
        prescriptionId = intent.getStringExtra("id");

        TextView title = (TextView) findViewById(R.id.toolbar_title);
        title.setText(date);


        ParseQuery<ParseObject> prescriptionQuery = ParseQuery.getQuery("prescriptions");
        prescriptionQuery.whereEqualTo("objectId", prescriptionId);

        try {
            prescriptionOb = prescriptionQuery.find().get(0);
        } catch (ParseException e) {
            Log.i("MedicineActivity","Prescription Query Failed. Reason: "+e.getMessage());
        }
        prescriptionDate = prescriptionOb.getCreatedAt();
        populateMedList();

    }
    
    public void populateMedList(){

        Medicines = new ArrayList<>();

        ParseQuery<ParseObject> medicineQuery = ParseQuery.getQuery("medicine");
        medicineQuery.whereEqualTo("prescriptionID", prescriptionId);
        medicineQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    Log.i("MedicineQuery","Count = "+objects.size());
                    for (ParseObject ob : objects) {
                        String medicineName = ob.get("name").toString();
                        int timesADay = ob.getInt("timesADay");
                        int numDays = ob.getInt("numDays");
                        calendar.setTime(prescriptionDate);
                        calendar.add(Calendar.DATE,numDays);
                        Date doneDate = calendar.getTime();
                        Log.i("MedicineQuery","Details: "+medicineName+" "+doneDate.toString());
                        Medicines.add(new Medicine(medicineName, timesADay,doneDate));
                        populateList();

                    }
                }
                else
                    Log.i("MedicineQuery","Error while Querying: "+e.getMessage());
            }
        });

    }

    public void populateList(){
        ArrayList<String> medNames = new ArrayList<>();
        ArrayList<String> medTimes = new ArrayList<>();
        ArrayList<String> medDate = new ArrayList<>();

        for(Medicine m: Medicines){
            medNames.add(m.getName());
            medTimes.add(m.getTimes());
            medDate.add(m.getDate());
        }
        ListView listView1 = (ListView) findViewById(R.id.medName);
        ListView listView2 = (ListView) findViewById(R.id.timesDay);
        ListView listView3 = (ListView) findViewById(R.id.endDate);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.list_view_style, medNames);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,
                R.layout.list_view_style, medTimes);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,
                R.layout.list_view_style, medDate);

        listView1.setAdapter(adapter);
        listView2.setAdapter(adapter1);
        listView3.setAdapter(adapter2);
    }

}
