package com.example.medicare.medicure;

import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by flazer on 14/11/2015.
 */
public class UserHandler {

    private ParseUser parseUser;
    private String username;
    private String fullname;
    private String DoB;
    private String bloodType;
    private String Allergies;
    private String emergencyName;
    private String emergencyNo;
    private String emergencyRel;
    private String role;

    public UserHandler(ParseUser user){
        this.parseUser = user;

    }

    public ParseUser getParseUser() {
        return parseUser;
    }



    public String getUsername() {
        this.username = parseUser.get("username").toString();
        return this.username;
    }
//
//    public void setUsername(String username) {
//        this.username = username;
//    }

    public String getFullname() {
        this.fullname = parseUser.get("name").toString();
        return this.fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
        parseUser.put("name",this.fullname);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler", "Full Name Changed and Commited");
            }
        });
    }

    public String getDoB() {
        Object user= parseUser.get("DateOfBirth");
        this.DoB = generateOutput(user);
        return this.DoB;
    }

    public void setDoB(String doB) {
        this.DoB = doB;
        parseUser.put("DateOfBirth",this.DoB);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler", "DateOfBirth Changed and Commited");
            }
        });
    }


    public String getBloodType() {
        Object user= parseUser.get("bloodType");
        this.bloodType = generateOutput(user);
        return bloodType;
    }

    public void setBloodType(String bloodtype) {
        this.bloodType = bloodtype;
        parseUser.put("bloodType",this.bloodType);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler","BloodType Changed and Commited");
            }
        });
    }

    public String getAllergies() {
        Object user = parseUser.get("allergies");
        this.Allergies = generateOutput(user);
        return this.Allergies;
    }

    public void setAllergies(String allergies) {
        this.Allergies = allergies;
        parseUser.put("allergies",this.Allergies);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler", "Allergies Changed and Commited");
            }
        });

    }

    public String getEmergencyName() {
        Object user = parseUser.get("EmergencyName");
        this.emergencyName = generateOutput(user);
        return this.emergencyName;

    }

    public void setEmergencyName(String emergencyName) {
        this.emergencyName = emergencyName;
        parseUser.put("EmergencyName",this.emergencyName);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler", "EmergencyName Changed and Commited");
            }
        });

    }

    public String getEmergencyNo() {
        Object user = parseUser.get("EmergencyNum");
        this.emergencyNo = generateOutput(user);
        return emergencyNo;
    }

    public void setEmergencyNo(String emergencyNo) {
        this.emergencyNo = emergencyNo;
        parseUser.put("EmergencyName",this.emergencyName);
        parseUser.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                Log.i("UserHandler", "EmergencyName Changed and Commited");
            }
        });

    }

    public String getEmergencyRel() {
        Object user = parseUser.get("EmergencyRel");
        this.emergencyRel = generateOutput(user);
        return emergencyRel;
    }

    public void setEmergencyRel(String emergencyRel) {
        this.emergencyRel = emergencyRel;
    }

    public String getRole() {
        return role;
    }

//    public void setRole(String role) {
//        this.role = role;
//    }

    public String generateOutput(Object object){

        if (object == null || object.toString().equals(""))
            return "This field is empty. Please populate it";
        else
            return object.toString();
    }




}
