package com.example.medicare.medicure;

public class Prescription {

    String date;
    String dr;
    String prescriptionID;

    public Prescription(String date, String dr, String id) {
        this.date = date;
        this.dr = dr;
        this.prescriptionID = id;
    }

    public String getDate(){
        return date;
    }

    public String getDR(){
        return dr;
    }

    public String getPrescriptionID() {return this.prescriptionID; }

}
