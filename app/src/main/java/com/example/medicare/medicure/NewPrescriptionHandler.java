package com.example.medicare.medicure;

import android.util.Log;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

/**
 * Created by flazer on 19/11/2015.
 */
public class NewPrescriptionHandler {

    private ParseUser parseUser;
    private ParseObject prescription;
    private String prescriptionID;
    private String patientUsername;
    private String doctorUsername;

    public NewPrescriptionHandler(String username){
        parseUser = ParseUser.getCurrentUser();
        prescription = new ParseObject("prescriptions");
        patientUsername = username;
        doctorUsername = parseUser.getUsername();
        prescription.put("doctor_username",doctorUsername);
        prescription.put("patient_username",patientUsername);
        try {
            ParseACL parseACL = new ParseACL(ParseUser.getCurrentUser());
            parseACL.setPublicReadAccess(true);
            prescription.setACL(parseACL);
            prescription.save();
            Log.i("NewPrescriptionHandler", "New Prescription created: " + prescription.getObjectId());
            prescriptionID = prescription.getObjectId();
        } catch (ParseException e) {
            Log.i("NewPrescriptionHandler", "Error :" + e.getMessage());
        }
    }

    public void addMedicine(String medName, String medTime, String medDate){
        ParseObject medicine = new ParseObject("medicine");
        ParseACL parseACL = new ParseACL(ParseUser.getCurrentUser());
        parseACL.setPublicReadAccess(true);
        medicine.setACL(parseACL);
        medicine.put("prescriptionID",prescriptionID);
        medicine.put("name",medName);
        medicine.put("timesADay", Integer.parseInt(medTime));
        medicine.put("numDays",Integer.parseInt(medDate));
        try {
            medicine.save();
            Log.i("NewPrescriptionHandler", "New medicine added");
        }
        catch (ParseException e) {
                Log.i("NewPrescriptionHandler","Error :"+e.getMessage());
        }
    }
}






