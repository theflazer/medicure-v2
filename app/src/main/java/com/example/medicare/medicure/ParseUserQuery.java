package com.example.medicare.medicure;

import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by flazer on 19/11/2015.
 */
public class ParseUserQuery {

    private String username;
    private String fullname;

    public ParseUserQuery(String input){
        this.username = input;
        queryUser();
    }

    public void queryUser(){
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        fullname = "";
        query.whereEqualTo("username", this.username);
        try {
            List<ParseUser> parseUsers = query.find();
            fullname = parseUsers.get(0).get("name").toString();
        }
        catch (ParseException e) {
            Log.i("ParseQueryUser", "User Query Unsuccessful: " + e.getMessage());
        }
    }

    public String getFullname(){
        return fullname;
    }
}
