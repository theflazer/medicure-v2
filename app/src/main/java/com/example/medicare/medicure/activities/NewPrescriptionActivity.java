package com.example.medicare.medicure.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.medicare.medicure.Medicine;
import com.example.medicare.medicure.NewPrescriptionHandler;
import com.example.medicare.medicure.R;

import java.util.ArrayList;

public class NewPrescriptionActivity extends AppCompatActivity {
    EditText medName;
    EditText medTime;
    EditText medDate;
    ImageView addButton;
    ListView listView;
    ArrayList<String> listItems;
    ArrayAdapter<String> adapter;
    ArrayList<Medicine> medicines;
    NewPrescriptionHandler newPrescriptionHandler;
    private Intent prevIntent;
    private String patient_username;
    Button confirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prevIntent = getIntent();
        patient_username = prevIntent.getStringExtra("patient_username");
        setContentView(R.layout.activity_new_prescription);
        medName = (EditText) findViewById(R.id.medName_input);
        medTime = (EditText) findViewById(R.id.medTime_input);
        medDate = (EditText) findViewById(R.id.medEnd_input);
        addButton = (ImageView) findViewById(R.id.addItem);
        listView = (ListView) findViewById(R.id.listView);
        listItems = new ArrayList<String>();
        medicines = new ArrayList<Medicine>();

        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(adapter);
        addButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {


                if (!medName.getText().toString().matches("") && !medTime.getText().toString().matches("") && !medDate.getText().toString().matches("")) {
                    String name = medName.getText().toString();
                    int times = Integer.parseInt(medTime.getText().toString());
                    int days = Integer.parseInt(medDate.getText().toString());
                    Medicine temp = new Medicine(name, times, days);
                    medicines.add(temp);
                    listItems.add(medName.getText().toString() + " " + medTime.getText().toString() + " time(s) a day for " + medDate.getText().toString() + " days");
                    medName.setText("");
                    medTime.setText("");
                    medDate.setText("");
                    adapter.notifyDataSetChanged();
                    View b = findViewById(R.id.confirm);
                    b.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getApplicationContext(), "Inputs cannot be empty", Toast.LENGTH_SHORT).show();
                    return;

                }


            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(NewPrescriptionActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Confirm Delete");

                // Setting Dialog Message
                alertDialog.setMessage("Are you sure you want delete this?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.plus);
                final int pos = position;

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        // Write your code here to invoke YES event

                        listItems.remove(pos);
                        medicines.remove(pos);
                        adapter.notifyDataSetChanged();
                        if (listItems.isEmpty()) {
                            View b = findViewById(R.id.confirm);
                            b.setVisibility(View.GONE);
                        }
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to invoke NO event
                        return;
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        confirmButton = (Button) findViewById(R.id.confirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newPrescriptionHandler = new NewPrescriptionHandler(patient_username);
                for (Medicine m:medicines){
                    newPrescriptionHandler.addMedicine(m.getName(),m.getTimes(),m.getN_days());
                }
                Intent i = new Intent(NewPrescriptionActivity.this, ReadQRCodeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });
    }
}