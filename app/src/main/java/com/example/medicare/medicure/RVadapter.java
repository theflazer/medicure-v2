package com.example.medicare.medicure;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.medicare.medicure.activities.MedicineActivity;

import java.util.List;

public class RVadapter extends RecyclerView.Adapter<RVadapter.PrescriptionViewHolder> {
    public static int position;
    Context mContext;
    public static String LOG_TAG = "Adapter";

    public class PrescriptionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cv;
        TextView PrescriptionName;
        TextView PrescriptionAge;

        PrescriptionViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.card_view);
            PrescriptionName = (TextView)itemView.findViewById(R.id.date);
            PrescriptionAge = (TextView)itemView.findViewById(R.id.dr);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {


            position = getAdapterPosition();
            String date_display = Prescriptions.get(position).getDate();
            String dr_display = Prescriptions.get(position).getDR();
            String prescriptionId = Prescriptions.get(position).getPrescriptionID();

            Intent i = new Intent(mContext, MedicineActivity.class);
            i.putExtra("date",date_display );
            i.putExtra("dr",dr_display );
            i.putExtra("id",prescriptionId);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(i);
        }
    }

    public List<Prescription> Prescriptions;

    public RVadapter(Context context, List<Prescription> Prescriptions){
        mContext = context;
        this.Prescriptions = Prescriptions;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PrescriptionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        PrescriptionViewHolder pvh = new PrescriptionViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(PrescriptionViewHolder PrescriptionViewHolder, int i) {
        PrescriptionViewHolder.PrescriptionName.setText(Prescriptions.get(i).date);
        PrescriptionViewHolder.PrescriptionAge.setText(Prescriptions.get(i).dr);
    }

    @Override
    public int getItemCount() {
        return Prescriptions.size();
    }


}