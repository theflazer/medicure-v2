package com.example.medicare.medicure.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.medicare.medicure.QRResources.QRDownloadController;
import com.example.medicare.medicure.R;
import com.parse.ParseUser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class QRGeneratorActivity extends AppCompatActivity {

    String parseUserID;
    ImageView qrImage;
    private static final int REQUEST_EXTERNAL = 0;
    private View mLayout;
    public static final String TAG = "QRGeneratorActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        parseUserID= ParseUser.getCurrentUser().getObjectId().toString();
        qrImage = (ImageView)findViewById(R.id.qrImage);
//        submitQRInfo = (Button)findViewById(R.id.submitQRInfo);

        QRDownloadController qrDownloadController = new QRDownloadController(parseUserID);
        qrDownloadController.setImageView(qrImage);
        qrDownloadController.putImage();



    }

    public void saveImage(View v){
        BitmapDrawable drawable = (BitmapDrawable) qrImage.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        String dir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM";
        File image = new File(dir, "qr.png");
        boolean success = false;

        // Encode the file as a PNG image.
        FileOutputStream outStream;
        try {

            outStream = new FileOutputStream(image);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream);
        /* 100 to keep full quality of the image */

            outStream.flush();
            outStream.close();
            success = true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (success) {
            Toast.makeText(getApplicationContext(), "Image saved to SD Card",
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getApplicationContext(),
                    "Error during image saving", Toast.LENGTH_LONG).show();
        }
    }

    public void saveExternal(View view) {
        Log.i(TAG, "Save Image button pressed. Checking permission.");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestExternalPermission();

        } else {

            Log.i(TAG,
                    "Write External permission has already been granted. Saving Image");
            saveImage(view);
        }

    }

    private void requestExternalPermission() {
        Log.i(TAG, "EXTERNAL permission has NOT been granted. Requesting permission.");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            Log.i(TAG,
                    "Displaying external storage rationale to provide additional context.");
            Snackbar.make(mLayout, "This app would like to access external storage",
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(QRGeneratorActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    REQUEST_EXTERNAL);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_EXTERNAL);
        }
    }

}
