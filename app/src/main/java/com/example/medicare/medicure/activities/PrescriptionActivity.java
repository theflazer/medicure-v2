package com.example.medicare.medicure.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.medicare.medicure.DateFormatter;
import com.example.medicare.medicure.ParseUserQuery;
import com.example.medicare.medicure.Prescription;
import com.example.medicare.medicure.R;
import com.example.medicare.medicure.RVadapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class PrescriptionActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private List<Prescription> Prescriptions;
    private RecyclerView rv;
    private NavigationView navigationView;
    private ParseObject parseObject;
    TextView navName;
    TextView navRole;
//    String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        Intent intent = getIntent();
//        username = intent.getExtras().getString("username");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.prescription_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        rv=(RecyclerView)findViewById(R.id.rv);
        parseObject = new ParseObject("prescriptions");
        View header = navigationView.inflateHeaderView(R.layout.nav_header_prescription);
        navName = (TextView) header.findViewById(R.id.nav_name);
        navRole = (TextView) header.findViewById(R.id.nav_role);
        String role = ParseUser.getCurrentUser().getString("role");
        navName.setText(ParseUser.getCurrentUser().getString("name"));
        navRole.setText(role.substring(0, 1).toUpperCase() + role.substring(1));
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
//        navigationView = (NavigationView)findViewById(R.id.prescription_nav_view);
//        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem item) {
//                if (item.getItemId() == R.id.prescription_nav_logout) {
//                    Log.i("LogOut", "Log Out Command issued");
//                    ParseUser.logOut();
//                    Intent logOutIntent = new Intent(PrescriptionActivity.this, LoginActivity.class);
//                    logOutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(logOutIntent);
//                    finish();
//                }
//                return true;
//            }
//        });


        initializeData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.prescription, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();




        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.prescription_nav_profile)
        {
            Intent i = new Intent(PrescriptionActivity.this, ProfilePageActivity.class);
            startActivity(i);
            finish();
        }
        else if (id == R.id.prescription_nav_prescriptions)
        {

        }
        else if(id == R.id.prescription_nav_logout){
            Log.i("LogOut", "Log Out Command issued");
            ParseUser.logOut();
            Intent logOutIntent = new Intent(PrescriptionActivity.this, LoginActivity.class);
            logOutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(logOutIntent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initializeData(){
        Prescriptions = new ArrayList<>();
        ParseQuery<ParseObject> prescriptionQuery = ParseQuery.getQuery("prescriptions");
        prescriptionQuery.whereContains("patient_username", ParseUser.getCurrentUser().getUsername());
        Log.i("PrescriptionQuery", "User: " + ParseUser.getCurrentUser().getUsername());
        prescriptionQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    Log.i("PrescriptionQuery","Querying Successful");
                    Log.i("PrescriptionQuery", "Number of Objects = "+objects.size());
                    for (ParseObject ob: objects) {
                        String date = new DateFormatter(ob.getCreatedAt()). getStringDate();
                        String name = new ParseUserQuery(ob.get("doctor_username").toString()).getFullname();
                        Prescriptions.add(new Prescription(date,name,ob.getObjectId()));
                        initializeAdapter();
                    }
                }
                else{
                    Log.i("PrescriptionQuery","Error while Querying: "+e.getMessage());
                }
            }
        });
    }

    private void initializeAdapter(){
        RVadapter adapter = new RVadapter(getApplicationContext(), Prescriptions);
        rv.setAdapter(adapter);
    }
}
