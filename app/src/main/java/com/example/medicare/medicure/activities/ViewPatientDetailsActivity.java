package com.example.medicare.medicure.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.medicare.medicure.R;
import com.parse.ParseException;
import com.parse.ParseUser;

public class ViewPatientDetailsActivity extends AppCompatActivity {

    private TextView fullname;
    private EditText allergies;
    private EditText emergencyName;
    private EditText emergencyNumber;
    private EditText bloodType;
    private EditText dob;
    private ImageView addPrescription;
    private ParseUser parseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_patient_details);
        Intent i = getIntent();
        String objectId = i.getStringExtra("objectId");

        Log.i("PatientDetailsActivity","Object Id: "+objectId);
        fullname = (TextView)findViewById(R.id.fullnameEditText);
        allergies = (EditText)findViewById(R.id.fordoc_alergies);
        emergencyName = (EditText)findViewById(R.id.fordoc_emergencyContact);
        emergencyNumber = (EditText)findViewById(R.id.fordoc_emergencyPhoneNumber);
        bloodType = (EditText)findViewById(R.id.fordoc_bloodtype);
        dob = (EditText)findViewById(R.id.profile_dob_fordoc);

        try {
            parseUser = ParseUser.getQuery().get(objectId);
        } catch (ParseException e) {
            Log.e("PatientDetailsAcitivity","Query could not be executed: "+e.getMessage());
        }
        fullname.setText(parseUser.getString("name"));
        allergies.setText(parseUser.getString("allergies"));
        emergencyName.setText(parseUser.getString("EmergencyName"));
        emergencyNumber.setText(parseUser.getString("EmergencyNum"));
        bloodType.setText(parseUser.getString("bloodType"));
        dob.setText(parseUser.getString("DateOfBirth"));

    }

    @Override
    public void onResume(){
        super.onResume();
        addPrescription = (ImageView)findViewById(R.id.addPrespcription);
        addPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ViewPatientDetailsActivity.this, NewPrescriptionActivity.class);
                i.putExtra("patient_username",parseUser.getUsername().toString());
                startActivity(i);
            }
        });
    }
}
