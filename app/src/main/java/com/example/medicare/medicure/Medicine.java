package com.example.medicare.medicure;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sohailyarkhan on 18/11/15.
 */
public class Medicine {
    private String name;
    private int times_a_day;
    DateFormat dateFormat= new SimpleDateFormat("dd/MM/yyyy");
    private Date end_date;
    private int n_days;
    String reportDate;

    public Medicine(String name, int times, Date date){
        this.name = name;
        this.times_a_day = times;
        this.end_date = date;
    }

    public Medicine(String name, int times, int days){
        this.name = name;
        this.times_a_day = times;
        this.n_days = days;
    }

    public String getName(){
        return name;
    }

    public String getTimes(){
        return String.valueOf(times_a_day);
    }

    public String getDate(){

        reportDate = dateFormat.format(end_date);
        return reportDate;

    }
    //Flazer use this getter
    public String getN_days(){
        return String.valueOf(n_days);
    }

    @Override
    public String toString() {

        reportDate = dateFormat.format(end_date);
        return this.name + "      " + this.times_a_day + "      " + this.reportDate;
    }
}
