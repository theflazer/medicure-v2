package com.example.medicare.medicure.activities;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.example.medicare.medicure.R;
import com.parse.ParseUser;


public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity

                if (ParseUser.getCurrentUser().getUsername() == null|| ParseUser.getCurrentUser().getUsername().equals("null")){
                    Intent i = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(i);
                }
                else {
                    if(ParseUser.getCurrentUser().get("role").equals("doctor")) {
                        Intent i = new Intent(SplashScreen.this, ReadQRCodeActivity.class);
                        startActivity(i);
                    }
                    else
                    {
                        Intent i = new Intent(SplashScreen.this, PrescriptionActivity.class);
                        startActivity(i);
                    }
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }


}