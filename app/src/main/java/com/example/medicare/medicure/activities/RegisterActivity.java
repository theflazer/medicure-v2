package com.example.medicare.medicure.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.medicare.medicure.R;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

@ParseClassName("Registration")
public class RegisterActivity extends AppCompatActivity {

    private EditText input_username;
    private EditText input_patient_password;
    private EditText input_name;
    private EditText input_dob;
    private EditText input_bloodtype;
    private EditText input_allergies;
    private EditText input_emergencyName;
    private EditText input_emergencyNo;
    private EditText input_emergencyRel;
    private String role;
    private Button btn_register;
    private ParseUser parseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        Intent intent = getIntent();
        initialize();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Parse", "Registering User");
                if(validateUser())
                {
                    parseUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                Log.i("ParseSignup", e.getMessage());
                                if (e.getCode() == 202)
                                    Toast.makeText(getApplicationContext(), "Sorry! Username " + input_username.getText().toString() + " has already been taken.", Toast.LENGTH_LONG).show();
                            } else {
                                Log.i("ParseSignup", "User Signed Up Succesfully");
                                if (e != null)
                                    Log.i("ParseSignupErrorSlip", e.getMessage());
                                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                                i.putExtra("reg", true);
                                setResult(RESULT_OK, i);
                                finish();
                            }
                        }
                    });

                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.validate_registration_warning, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initialize()
    {
        input_username = (EditText)findViewById(R.id.input_username);
        input_patient_password = (EditText)findViewById(R.id.input_patient_password);
        input_name = (EditText)findViewById(R.id.input_name);
        input_dob = (EditText)findViewById(R.id.input_dob);
        input_bloodtype = (EditText)findViewById(R.id.input_bloodtype);
        input_allergies = (EditText)findViewById(R.id.input_allergies);
        input_emergencyName = (EditText)findViewById(R.id.input_emergencyName);
        input_emergencyNo = (EditText)findViewById(R.id.input_emergencyNo);
        input_emergencyRel = (EditText)findViewById(R.id.input_emergencyRel);
        btn_register = (Button)findViewById(R.id.btn_register);
        parseUser = new ParseUser();
        ParseUser currentUser = ParseUser.getCurrentUser();
        currentUser.logOut();
    }

    private boolean validateUser()
    {
        boolean valid=true;
        if(input_username.getText().toString()!=null&&!input_username.getText().toString().equals(""))
            parseUser.setUsername(input_username.getText().toString());
        else
            valid=false;

        if(input_patient_password.getText().toString()!=null&&!input_patient_password.getText().toString().equals(""))
            parseUser.setPassword(input_patient_password.getText().toString());
        else
            valid=false;

        if(input_name.getText().toString()!=null&&!input_name.getText().toString().equals(""))
            parseUser.put("name", input_name.getText().toString());
        else
            valid=false;

        if(input_dob.getText().toString()!=null&&!input_dob.getText().toString().equals(""))
            parseUser.put("DateOfBirth", input_dob.getText().toString());
        else
            valid=false;

        if(input_bloodtype.getText().toString()!=null&&!input_bloodtype.getText().toString().equals(""))
            parseUser.put("bloodType", input_bloodtype.getText().toString());
        else
            valid=false;

        if(input_allergies.getText().toString()!=null&&!input_allergies.getText().toString().equals(""))
            parseUser.put("allergies", input_allergies.getText().toString());
        else
            valid=false;

        if(input_emergencyName.getText().toString()!=null&&!input_emergencyName.getText().toString().equals(""))
        parseUser.put("EmergencyName", input_emergencyName.getText().toString());
        else
            valid=false;

        if(input_emergencyNo.getText().toString()!=null&&!input_emergencyNo.getText().toString().equals(""))
        parseUser.put("EmergencyNum", input_emergencyNo.getText().toString());
        else
            valid=false;

        if(input_emergencyRel.getText().toString()!=null&&!input_emergencyRel.getText().toString().equals(""))
        parseUser.put("EmergencyRel", input_emergencyRel.getText().toString());
        else
            valid=false;

        if(role!=null)
        parseUser.put("role", role);
        else
            valid=false;

        return valid;
    }

    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();
        this.role = "patient";
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_doctor:
                if (checked)
                    this.role="doctor";
                    break;
            case R.id.radio_patient:
                if(checked)
                    this.role="patient";
                    break;
            }

        }

    }
