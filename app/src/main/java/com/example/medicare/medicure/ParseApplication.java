package com.example.medicare.medicure;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * Created by flazer on 14/11/2015.
 */
public class ParseApplication extends Application{

    @Override
    public void onCreate(){
        super.onCreate();
        Parse.enableLocalDatastore(this);

        // Add your initialization code here
        Parse.initialize(getApplicationContext(), "be9xgsuR6dgwWWw8Krh7w4IcrzAXIhI6ofHpFo4S", "pbRnaNpHjMdKsVYcq2Kc18IeI8qQSSguRRY7j4wK");

        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
        ParseACL.setDefaultACL(defaultACL, true);


    }
}
