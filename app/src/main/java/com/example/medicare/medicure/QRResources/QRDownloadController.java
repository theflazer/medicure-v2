package com.example.medicare.medicure.QRResources;

import android.util.Log;
import android.widget.ImageView;

/**
 * Created by flazer on 8/11/2015.
 */
public class QRDownloadController {


    private ImageView imageView;
    private String text;
    private String apiURL;
    private ImageManipulator imageDownloader;

    public QRDownloadController(String textToConvert) {
        this.text = textToConvert;
        if (this.text==null || this.text=="")
            Log.e("emptyText", "The String provided is an empty string");
        this.apiURL = "https://api.qrserver.com/v1/create-qr-code/?size=400x400&data="+text;
        this.imageDownloader = new ImageManipulator();
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
        imageDownloader.download(apiURL,imageView);
    }

    public void putImage() {
        if (imageView==null)
            imageDownloader.display();
    }
}
